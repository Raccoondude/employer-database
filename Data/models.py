from django.db import models
from django.core.validators import MinValueValidator

# Create your models here.

class Employer(models.Model):
    Employer_ID = models.AutoField(primary_key = True)
    Employer_name = models.CharField(max_length=25)
    Employer_age = models.IntegerField(default=18)
    sex = (
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('Other', 'Other')
        )
    Employer_sex = models.CharField(choices=sex, max_length=10)
    title = (
        ('CEO', 'CEO'),
        ('Manger', 'Manger'),
        ('Other', 'Other')
        )
    Employer_title = models.CharField(choices=title, max_length=7)
    def __str__(self):
        return self.Employer_name

class Worker(models.Model):
    Worker_ID = models.AutoField(primary_key = True)
    Worker_name = models.CharField(max_length=25)
    Worker_age = models.IntegerField(default=18)
    sex = (
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('Other', 'Other')
        )
    Worker_sex = models.CharField(choices=sex, max_length=10)
    Worker_pay = models.FloatField(validators=[MinValueValidator(7.25)])
    Worker_employer = models.ForeignKey(Employer, on_delete=models.CASCADE)
    def __str__(self):
        return self.Worker_name
