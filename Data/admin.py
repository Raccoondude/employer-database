from django.contrib import admin
from .models import Worker, Employer

# Register your models here.

admin.site.register(Worker)
admin.site.register(Employer)
