from django.shortcuts import render
from django.http import HttpResponse
from .models import Employer, Worker

# Create your views here.

def index(request):
    OwO = Worker.objects.all()
    UwU = Employer.objects.all()
    return render(request, 'Data/index.html', {'worker_list':OwO, 'employer_list':UwU})

def view_worker(request, url_id):
    OwO = Worker.objects.get(Worker_ID=url_id)
    name = OwO.Worker_name
    age = OwO.Worker_age
    sex = OwO.Worker_sex
    pay = OwO.Worker_pay
    employer = OwO.Worker_employer
    return render(request, 'Data/worker.html', {'name':name, 'age':age, 'sex':sex, 'pay':pay, 'employer':employer})

def view_employer(request, url_id):
    OwO = Employer.objects.get(Employer_ID=url_id)
    name = OwO.Employer_name
    age = OwO.Employer_age
    sex = OwO.Employer_sex
    title = OwO.Employer_title
    return render(request, 'Data/employer.html', {'name':name, 'age':age, 'sex':sex, 'title':title})
