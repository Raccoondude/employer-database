from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('worker/<int:url_id>', views.view_worker, name='view_worker'),
    path('employer/<int:url_id>', views.view_employer, name='view_employer'),
]
